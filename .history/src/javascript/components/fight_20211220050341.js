import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    controls.PlayerOneAttack = getDamage(firstFighter.attack, secondFighter.defense);
    console.log(resolve);
    let firstHealth = firstFighter.health;
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  // return damage
  return getHitPower(attacker) - getBlockPower(defender);
}

export function getHitPower(fighter) {
  const attack = fighter.attack;

  function criticalHitChance(min, max) {
    return Math.random() * (max - min) + min;
  }
  const power = attack * criticalHitChance(1, 2);
  // return hit power
  console.log(power, 'power');
  return power;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;

  function dodgeChance(min, max) {
    return Math.random() * (max - min) + min;
  }
  const power = defense * dodgeChance(1, 2);
  // return block power
  return power;
}
