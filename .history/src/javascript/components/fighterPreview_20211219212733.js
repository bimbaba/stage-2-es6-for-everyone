import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  const { name, health, attack, defense } = fighter;
  const figtherAttributes = createElement({
    name: name,
    health: health
  });

  const fighterInfo = createElement({
    tagName: 'div',
    className: `fighter-info___root`,
    figtherAttributes
  });

  const imgElement = createFighterImage(fighter);
  fighterElement.append(fighterInfo, imgElement);
  fighterInfo.append(imgElement, name, health, attack, defense);

  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
