import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  // return damage
}

export function getHitPower(fighter) {
  function criticalHitChance(min, max) {
    return Math.random() * (max - min) + min;
  }
  const attack = fighter.attack;
  let power = attack * criticalHitChance(1, 2);
  // return hit power
  console.log(power, 'power');
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense;
}
