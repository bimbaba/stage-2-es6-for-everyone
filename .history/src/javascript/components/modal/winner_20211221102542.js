import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: 'The WIN',
    bodyElement: fighter.name
  };
  showModal(winnerInfo);
}
