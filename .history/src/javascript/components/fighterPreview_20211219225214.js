import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  const { name, health, attack, defense } = fighter;

  const fighterInfo = createElement({
    tagName: 'div',
    className: `fighter-info___root`
  });

  const imgElement = createFighterImage(fighter);
  const fighterContent = createElement({
    tagName: 'div',
    className: `fighter-info___content ${positionClassName}`
  });
  const fighterName = createElement({ tagName: 'span', className: 'fighter-info___fighter-name' });
  const fighterHealth = createElement({ tagName: 'span', className: 'fighter-info___fighter-health' });
  const fighterAttack = createElement({ tagName: 'span', className: 'fighter-info___fighter-attack' });
  fighterElement.append(imgElement, fighterContent);
  fighterContent.append(fighterInfo);
  fighterInfo.append(fighterName, fighterHealth, attack, defense);

  fighterName.innerText = name;
  fighterHealth.innerText = `Health ${health}`;
  fighterAttack.innerText = `Attack ${attack}`;
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
